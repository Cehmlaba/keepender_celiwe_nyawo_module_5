import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class List extends StatefulWidget {
  const List({Key? key}) : super(key: key);

  @override
  State<List> createState() => _ListState();
}

class _ListState extends State<List> {
  final Stream<QuerySnapshot> myform =
      FirebaseFirestore.instance.collection("form").snapshots();
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: myform,
        builder: (BuildContext context,
            AsyncSnapshot<QuerySnapshot<Object?>> snapshot) {
          if (snapshot.hasError) {
            return Text("Snapshot has Errors");
          }
          if (snapshot.connectionState == ConnectionState.waiting) {
            return CircularProgressIndicator();
          }
          if (snapshot.hasData) {
            return Row(
              children: [
                Expanded(
                    child: SizedBox(
                        height: (MediaQuery.of(context).size.height),
                        width: (MediaQuery.of(context).size.width),
                        child: ListView(
                          children: snapshot.data!.docs
                              .map((DocumentSnapshot documentSnapshot) {
                            Map<String, dynamic> data = documentSnapshot.data()!
                                as Map<String, dynamic>;
                            return Column(
                              children: [
                                Card(
                                  child: Column(children: [
                                    ListTile(
                                      title: Text(data['name']),
                                      subtitle: Text(data['Location']),
                                    ),
                                    ButtonTheme(
                                        child: ButtonBar(
                                      children: [
                                        OutlineButton.icon(
                                            onPressed: () {},
                                            icon: Icon(Icons.edit),
                                            label: Text("Edit")),
                                        OutlineButton.icon(
                                            onPressed: () {},
                                            icon: Icon(Icons.remove),
                                            label: Text("Delete"))
                                      ],
                                    ))
                                  ]),
                                )
                              ],
                            );
                          }).toList(),
                        )))
              ],
            );
          } else {
            return Text(("No Data"));
          }
        });
  }
}
