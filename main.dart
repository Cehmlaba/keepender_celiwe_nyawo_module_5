import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:module3/splashscreen.dart';
import 'package:firebase_core/firebase_core.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
      options: FirebaseOptions(
          apiKey: "AIzaSyDbhrle53aMJjM9EMbkwvuHvyBt9ZH5roo",
          authDomain: "module-5-8fb95.firebaseapp.com",
          projectId: "module-5-8fb95",
          storageBucket: "module-5-8fb95.appspot.com",
          messagingSenderId: "106580231214",
          appId: "1:106580231214:web:446b3fbd6f845bcf6b8bbf"));
  SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(statusBarColor: Colors.transparent));
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Splash Screen',
        home: AnimatedSplashScreen(),
        theme: ThemeData(
            primarySwatch: Colors.lightBlue,
            accentColor: Colors.blueAccent,
            scaffoldBackgroundColor: Colors.grey));
  }
}
