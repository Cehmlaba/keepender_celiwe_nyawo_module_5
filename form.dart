import 'dart:developer';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class Forms extends StatefulWidget {
  const Forms({Key? key}) : super(key: key);

  @override
  State<Forms> createState() => _FormsState();
}

class _FormsState extends State<Forms> {
  @override
  Widget build(BuildContext context) {
    TextEditingController nameContoller = TextEditingController();
    TextEditingController LocationContoller = TextEditingController();
    TextEditingController OTPContoller = TextEditingController();

    Future _forms() {
      final name = nameContoller.text;
      final Location = LocationContoller.text;
      final OTP = OTPContoller.text;

      final ref = FirebaseFirestore.instance.collection("form").doc();
      return ref
          .set({
            "name": name,
            "Location": Location,
            "OTP": OTP,
            "doc_id": ref.id
          })
          .then((value) => log("Collection Added!!"))
          .catchError((onError) => log(onError));
    }

    return Column(
      children: [
        Padding(
            padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
            child: Material(
                child: TextField(
              controller: nameContoller,
              decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20)),
                  hintText: "Enter your name"),
            ))),
        Padding(
            padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
            child: Material(
              child: TextField(
                controller: LocationContoller,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20)),
                    hintText: "Enter Location"),
              ),
            )),
        Padding(
            padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
            child: Material(
              child: TextField(
                controller: OTPContoller,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20)),
                    hintText: "Enter OTP"),
              ),
            )),
        ElevatedButton(onPressed: () => {_forms}, child: Text("Join Session")),
      ],
    );
  }
}
